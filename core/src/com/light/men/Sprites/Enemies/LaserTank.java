package com.light.men.Sprites.Enemies;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;
import com.light.men.Constants.GameConstants;
import com.light.men.LightMen;
import com.light.men.Screens.PlayScreen;
import com.light.men.Sprites.Player;

/**
 * Created by Admin on 24.07.2017.
 */

public class LaserTank extends com.light.men.Sprites.Enemies.Enemy {


    private boolean setToDestroy;
    public  boolean  destroyed;
    private float stateTime;
    private State currentState;
    private float bulletTimer;
    private float animTimer;
    private box2dLight.PointLight orangeLight;
    private box2dLight.PointLight lightgrayLight;
    private box2dLight.ConeLight coneLightR;
    private box2dLight.ConeLight coneLightL;
    private Animation driveAnim;
    private TankBullet tankBullet;

    public static boolean left,right;


    private final TextureRegion lasergunNormal;
    private final TextureRegion lasergunDead;
    private final TextureRegion lasergunShooting;

    private boolean shoot;
    private  Array<TextureRegion> frames;
    public enum State {
        SHOOTING,DEAD,DRIVING,STANDING
    }



    public LaserTank(PlayScreen screen, float x, float y) {
        super(screen, x, y);
        setBounds(getX(),getY(),16 / LightMen.PPM,16 / LightMen.PPM);
        setToDestroy = false;
        destroyed = false;
        shoot = false;
        lightgrayLight = new box2dLight.PointLight(screen.getRayHandler(),GameConstants.rayCount, Color.LIGHT_GRAY,0.25f,x,y);
        lightgrayLight.setIgnoreAttachedBody(true);
        lightgrayLight.attachToBody(enemybody);

        orangeLight = new box2dLight.PointLight(screen.getRayHandler(),GameConstants.rayCount, Color.ORANGE,0.57f,x,y);
        orangeLight.setIgnoreAttachedBody(true);
        orangeLight.attachToBody(enemybody);
        orangeLight.setXray(true);
        enemybody.setSleepingAllowed(false);

        coneLightR = new box2dLight.ConeLight(screen.getRayHandler(), GameConstants.rayCount, Color.RED, 1.25f, enemybody.getPosition().x, enemybody.getPosition().y + 4 / LightMen.PPM, 0, 25);
        coneLightR.setActive(false);
        coneLightR.setXray(true);


        coneLightL = new box2dLight.ConeLight(screen.getRayHandler(), GameConstants.rayCount, Color.RED, 1.25f, enemybody.getPosition().x, enemybody.getPosition().y + 4 / LightMen.PPM, 180, 25);
        coneLightL.setActive(false);
        coneLightL.setXray(true);

        lasergunNormal = new TextureRegion(screen.getAtlas().findRegion("lasertank"), 0, 0, 16, 16);
        lasergunDead = new TextureRegion(screen.getAtlas().findRegion("lasertank"), 48, 0, 16, 16);
        lasergunShooting = new TextureRegion(screen.getAtlas().findRegion("lasertank"), 64, 0, 16, 16);

        frames = new Array<TextureRegion>();
        for(int i = 0; i < 3; i++) {
            frames.add(new TextureRegion(screen.getAtlas().findRegion("lasertank"),i*16,0,16,16));

        }
        driveAnim = new Animation(0.4f, frames);
        frames.clear();

        animTimer = 0;
        bulletTimer=0;

    }

    @Override
    protected void defineEnemy() {

        BodyDef bdef = new BodyDef();
        bdef.position.set(getX(),getY());
        bdef.type = BodyDef.BodyType.DynamicBody;
        enemybody = world.createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(5/ LightMen.PPM);
        fdef.filter.categoryBits = LightMen.ENEMY_BIT;
        fdef.filter.maskBits = LightMen.GROUND_BIT
                | LightMen.COIN_BIT
                | LightMen.BRICK_BIT
                | LightMen.ENEMY_BIT
                | LightMen.PLAYER_BIT;

        fdef.shape = shape;
        enemybody.createFixture(fdef).setUserData(this);

        //create the Head here
        PolygonShape head = new PolygonShape();
        Vector2[] vertice = new Vector2[4];
        vertice[0] = new Vector2(-5,8).scl(1/LightMen.PPM);
        vertice[1] = new Vector2(5,8).scl(1/LightMen.PPM);
        vertice[2] = new Vector2(-3,3).scl(1/LightMen.PPM);
        vertice[3] = new Vector2(3,3).scl(1/LightMen.PPM);
        head.set(vertice);

        fdef.shape = head;
        fdef.restitution = 0.5f;
        fdef.filter.categoryBits = LightMen.ENEMY_HEAD_BIT;

        enemybody.createFixture(fdef).setUserData(this);


    }


    @Override
    public void update(float deltaTime) {

            stateTime += deltaTime;
            animTimer += deltaTime;
            bulletTimer += deltaTime;
            setRegion(getFrame());
            if (setToDestroy && !destroyed) {
                world.destroyBody(enemybody);
                orangeLight.remove();
                coneLightR.remove();
                coneLightL.remove();
                lightgrayLight.remove();
                destroyed = true;
                stateTime = 0;
                animTimer = 0;
                bulletTimer = 0;

            } else if (!destroyed) {
                if (!shoot) {
                    if (!currentState.equals(LaserTank.State.STANDING)) {
                        if (screen.getPlayer().b2body.getPosition().x < enemybody.getPosition().x) {
                            enemybody.setLinearVelocity(new Vector2(-velocity.x, velocity.y));
                            left = true;
                            right = false;
                        } else {
                            enemybody.setLinearVelocity(velocity);
                            right = true;
                            left = false;
                        }
                    }
                    setPosition(enemybody.getPosition().x - getWidth() / 2, (enemybody.getPosition().y - getHeight() / 2) + 2 / LightMen.PPM);

                } else {


                    enemybody.setLinearVelocity(new Vector2(0, 0));

                }
                if (tankBullet != null) {
                    tankBullet.update(deltaTime);
                }


                if (stateTime >= 7.5f && !shoot) {
                    orangeLight.setActive(true);
                    if (left) {
                        coneLightL.setActive(true);
                        coneLightL.setPosition(new Vector2(enemybody.getPosition().x, enemybody.getPosition().y));

                        if(enemybody.isActive()) {
                            tankBullet = new TankBullet(screen, enemybody.getPosition().x - 4 / LightMen.PPM, enemybody.getPosition().y, "left");
                            System.out.println("SHOOT");
                        }
                    } else {
                        coneLightR.setActive(true);
                        coneLightR.setPosition(new Vector2(enemybody.getPosition().x, enemybody.getPosition().y));
                        if(enemybody.isActive()) {
                            tankBullet = new TankBullet(screen, enemybody.getPosition().x + 4 / LightMen.PPM, enemybody.getPosition().y, "right");
                            System.out.println("SHOOT");
                        }
                    }

                    stateTime = 0;
                    shoot = true;

                    if (bulletTimer >= 15) {
                        bulletTimer = 0;
                        tankBullet.remove();
                    }


                }
                if (stateTime >= 1.25f) {
                    shoot = false;
                    bulletTimer = 0;

                    orangeLight.setActive(false);
                    if (left) {
                        coneLightL.setActive(false);
                    } else {
                        coneLightR.setActive(false);
                    }
                }


            }
        }



    @Override
    public void hitOnHead(Player player) {
        setToDestroy = true;
    }

    public void draw(Batch batch) {
        if(!destroyed ||stateTime < 1) {
            super.draw(batch);

                if(tankBullet != null) {
                    tankBullet.draw(batch);
                }

        }
    }

    @Override
    public void hitByEnemy(com.light.men.Sprites.Enemies.Enemy enemy) {
        reverseVelocity(true, false);
    }

    private TextureRegion getFrame() {

        currentState = getState();

        TextureRegion region = null;

        switch(currentState) {
            case DEAD:
                region = lasergunDead;
                break;
            case SHOOTING:
                region = lasergunShooting;
                if(screen.getPlayer().b2body.getPosition().x > enemybody.getPosition().x && !region.isFlipX()) {
                    region.flip(true,false);
                }else if(screen.getPlayer().b2body.getPosition().x < enemybody.getPosition().x && region.isFlipX()) {
                    region.flip(true, false);
                }
                break;
            case DRIVING:
                region = (TextureRegion) driveAnim.getKeyFrame(animTimer, true);
                if(screen.getPlayer().b2body.getPosition().x > enemybody.getPosition().x && !region.isFlipX()) {
                    region.flip(true,false);
                    right = true;
                    left = false;
                }else if(screen.getPlayer().b2body.getPosition().x < enemybody.getPosition().x && region.isFlipX()) {
                    region.flip(true,false);
                    right = false;
                    left = true;
                }
              break;
            case STANDING:
                region = lasergunNormal;
                break;
        }

        return region;
    }

    private LaserTank.State getState() {
        if(destroyed) {
            return State.DEAD;
        }else if(shoot){
           return State.SHOOTING;
        }else if(enemybody.getPosition().x - 8/LightMen.PPM <= screen.getPlayer().b2body.getPosition().x && enemybody.getPosition().x + 8/LightMen.PPM >= screen.getPlayer().b2body.getPosition().x) {
            System.out.println("STANDING");
            return State.STANDING;
        } else {
            return State.DRIVING;
        }
    }

    public void dispose() {
        coneLightR.dispose();
        coneLightL.dispose();
        orangeLight.dispose();
        lightgrayLight.dispose();
    }







}
