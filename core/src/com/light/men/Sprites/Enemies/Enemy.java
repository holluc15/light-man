package com.light.men.Sprites.Enemies;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.light.men.Screens.PlayScreen;
import com.light.men.Sprites.Player;


public abstract class Enemy extends Sprite {

    final World world;
    final PlayScreen screen;
    public Body enemybody;
    Vector2 velocity;
    protected TiledMap map;

    public Enemy(PlayScreen screen, float x, float y) {
        this.world = screen.getWorld();
        this.screen = screen;
        this.map = screen.getMap();
        setPosition(x,y);
        defineEnemy();
        velocity = new Vector2(-1,-2);
        if(enemybody != null) {
            enemybody.setActive(false);
        }



    }

    protected abstract void defineEnemy();
    public abstract void update(float deltaTime);
    public abstract void hitOnHead(Player player);
    public abstract void hitByEnemy(Enemy enemy);

    public void reverseVelocity(boolean x,boolean y) {
        if(x) {
            velocity.x = -velocity.x;
        }
        if(y) {
            velocity.y = -velocity.y;
        }
    }






}
