package com.light.men.Sprites.Blocks;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.light.men.Input.Controller;
import com.light.men.Items.ItemDef;
import com.light.men.Items.UseableItems.Star;
import com.light.men.LightMen;
import com.light.men.Screens.PlayScreen;
import com.light.men.Sprites.Player;


/**
 * Created by Admin on 21.06.2017.
 */

public class Coin extends InteractiveTileObject {

    private TiledMapTileSet tileSet;


    public Coin(PlayScreen screen, MapObject object) {
        super(screen, object);
        tileSet = map.getTileSets().getTileSet("tileset1"); //Enter Tileset name without .tsx
        fixture.setUserData(this);
        setCategoryFilter(LightMen.COIN_BIT);
    }


    @Override
    public void onHeadHit(Player player) {

        int BLANK_COIN = 5;
        if(getCell().getTile().getId() == BLANK_COIN) {
            LightMen.manager.get("audio/sounds/bump.wav",Sound.class).play();
        }else{
            if(object.getProperties().containsKey("star")) {
                screen.spawnItem(new ItemDef(new Vector2(body.getPosition().x ,body.getPosition().y + 16 / LightMen.PPM), Star.class));
            }
            LightMen.manager.get("audio/sounds/coin.wav",Sound.class).play();
            getCell().setTile(tileSet.getTile(BLANK_COIN));
        }


    }

    @Override
    public void dispose() {
        //nothing dispoable
    }

    @Override
    public void update() {

    }




}
