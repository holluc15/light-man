package com.light.men.Items;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by Admin on 22.06.2017.
 */

public class ItemDef {
    public final Vector2 position;
    public final Class<?> type;

    public ItemDef(Vector2 position, Class<?> type) {
        this.position = position;
        this.type = type;
    }
}
