package com.light.men.Sprites.MapObjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.light.men.Constants.GameConstants;
import com.light.men.LightMen;
import com.light.men.Screens.PlayScreen;

/**
 * Created by Admin on 18.08.2017.
 */

public class Platform extends MapObjects {


    private box2dLight.PointLight whiteLight;
    public int type;

    public Platform(PlayScreen screen, float x, float y,int type) {
        super(screen,x,y);
        this.type = type;

        setBounds(getX(),getY(),48 / LightMen.PPM,16 / LightMen.PPM);
        setRegion(new TextureRegion(screen.getAtlas().findRegion("platform"), 0, 0, 48, 16));
        velocity.set(0,0);

        whiteLight = new box2dLight.PointLight(screen.getRayHandler(), GameConstants.rayCount, Color.MAROON,0.7f,0,0);
        whiteLight.setXray(true);
        whiteLight.attachToBody(objectBody,18 / LightMen.PPM,8 / LightMen.PPM);

    }


    @Override
    protected void defineObject() {

        BodyDef bdef = new BodyDef();
        bdef.position.set(getX(),getY());
        bdef.type = BodyDef.BodyType.KinematicBody;
        objectBody = world.createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        PolygonShape shape = new PolygonShape();
        Vector2[] vertice = new Vector2[4];
        vertice[0] = new Vector2( 0, 0).scl(1/LightMen.PPM);
        vertice[1] = new Vector2( 0,16).scl(1/LightMen.PPM);
        vertice[2] = new Vector2(48, 0).scl(1/LightMen.PPM);
        vertice[3] = new Vector2(48,16).scl(1/LightMen.PPM);
        shape.set(vertice);
        fdef.filter.categoryBits = LightMen.PLATFORM_BIT;
        fdef.filter.maskBits = LightMen.PLAYER_BIT
                | LightMen.GROUND_BIT
                | LightMen.PLATFORM_BIT;

        fdef.shape = shape;
        objectBody.createFixture(fdef).setUserData(this);
    }

    public void start() {
        velocity.set(0.5f,0);
    }

    public void stop() {
        velocity.set(0,0);
    }

    @Override
    public void update(float deltaTime) {
        objectBody.setLinearVelocity(velocity);
            setPosition(objectBody.getPosition().x, objectBody.getPosition().y);
    }

    public void draw(Batch batch) {
            super.draw(batch);
    }

    public int getType() {
        return type;
    }

}
