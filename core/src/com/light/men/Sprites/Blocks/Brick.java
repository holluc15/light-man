package com.light.men.Sprites.Blocks;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.math.Rectangle;
import com.light.men.LightMen;
import com.light.men.Screens.PlayScreen;
import com.light.men.Sprites.Player;

/**
 * Created by Admin on 21.06.2017.
 */


public class Brick extends InteractiveTileObject {

    public Brick(PlayScreen screen, MapObject object) {
        super(screen, object);
        fixture.setUserData(this);
        setCategoryFilter(LightMen.BRICK_BIT);


    }

    @Override
    public void onHeadHit(Player player) {

        setCategoryFilter(LightMen.DESTROYED_BIT);
            getCell().setTile(null);
            LightMen.manager.get("audio/sounds/breakblock.wav", Sound.class).play();

    }

    @Override
    public void dispose() {
        //nothing dispoable
    }

    @Override
    public void update() {

    }




}