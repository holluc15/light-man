package com.light.men.Constants;

import com.badlogic.gdx.Gdx;

/**
 * Created by Lukas on 28.06.2017.
 */

public class GameConstants {

    public static final int screenWidth = Gdx.graphics.getWidth();
    public static final int screenHeight = Gdx.graphics.getHeight();
    public static final int col_width = screenWidth/8;
    public static final int row_height = screenWidth/8;
    public static boolean playerIsDead = false;
    public static boolean playerStarLight = false;
    public static boolean playerJumpsHigher = false;
    public static int rayCount = 20;
    public static boolean playerShadow = false;
    public static boolean isPlaying = true;





    // Game Configuration booleans
    public static boolean enemiesActive = true;
    public static boolean testButton = false;
    public static boolean lights = true;
    public static boolean renderOnlyLines = false;
    public static boolean renderLines = false;


}
