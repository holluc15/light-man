package com.light.men.Items.UseableItems;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.light.men.Constants.GameConstants;
import com.light.men.Items.Item;
import com.light.men.LightMen;
import com.light.men.Screens.PlayScreen;
import com.light.men.Sprites.Player;

import box2dLight.RayHandler;

public class Emerald extends Item {

    private final box2dLight.PointLight light;
    private final box2dLight.PointLight light2;

    public Emerald(PlayScreen screen, float x, float y) {
        super(screen, x, y);
        setRegion(screen.getAtlas().findRegion("emerald"),0,0,16,16);
        velocity = new Vector2(0.7f,0);
        light = new box2dLight.PointLight(screen.getRayHandler(),145, Color.GREEN,0.75f,x,y);
        light2 = new box2dLight.PointLight(screen.getRayHandler(),4, Color.CORAL,0.75f,x,y);
        light.setIgnoreAttachedBody(true);
        light.attachToBody(itemBody);
        light2.setIgnoreAttachedBody(true);
        light2.attachToBody(itemBody);

    }


    @Override
    public void defineItem() {
        BodyDef bdef = new BodyDef();
        bdef.position.set(getX(),getY());
        bdef.type = BodyDef.BodyType.DynamicBody;
        itemBody = world.createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(6/ LightMen.PPM);
        fdef.filter.categoryBits = LightMen.ITEM_BIT;
        fdef.filter.maskBits = LightMen.PLAYER_BIT
                | LightMen.GROUND_BIT;


        fdef.shape = shape;
        itemBody.createFixture(fdef).setUserData(this);
    }

    @Override
    public void use(Player player) {

        try {
            light.remove(true);
            light2.remove(true);
            GameConstants.isPlaying = false;
        }catch(NullPointerException ex) {
            ex.printStackTrace();
        }
        destroy();

    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        setPosition(itemBody.getPosition().x - getWidth()/ 2,itemBody.getPosition().y -getHeight()/ 2);
        velocity.y = itemBody.getLinearVelocity().y;
        itemBody.setLinearVelocity(velocity);
    }

}