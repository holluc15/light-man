package com.light.men.Items.UseableItems;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.utils.Array;
import com.light.men.Items.Item;
import com.light.men.LightMen;
import com.light.men.Screens.PlayScreen;
import com.light.men.Sprites.Player;
import com.light.men.Tools.LightMixer;

public class Star extends Item {


    private final box2dLight.PointLight light;
    private final box2dLight.PointLight redlight;
    private LightMixer lightmixer;
    private Animation starAnimation;
    private Array<TextureRegion> frames;
    private float stateTime;

    public Star(PlayScreen screen, float x, float y) {
        super(screen, x, y);
        velocity = new Vector2(0.7f,0);
        frames = new Array<TextureRegion>();
        for(int i = 0; i < 3; i++) {
            frames.add(new TextureRegion(screen.getAtlas().findRegion("star"), 16*i,0,16,16));
        }
        starAnimation = new Animation(0.4f, frames);
        light = new box2dLight.PointLight(screen.getRayHandler(),12, Color.YELLOW,0.45f,x,y);
        light.attachToBody(itemBody);
        redlight = new box2dLight.PointLight(screen.getRayHandler(),12, Color.RED,0.375f,x,y);
        redlight.attachToBody(itemBody);

        lightmixer = new LightMixer(screen);
        stateTime = 0;
    }


    @Override
    public void defineItem() {
        BodyDef bdef = new BodyDef();
        bdef.position.set(getX(),getY());
        bdef.type = BodyDef.BodyType.DynamicBody;
        itemBody = world.createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(6/ LightMen.PPM);
        fdef.filter.categoryBits = LightMen.ITEM_BIT;
        fdef.filter.maskBits = LightMen.PLAYER_BIT
                | LightMen.GROUND_BIT
                | LightMen.COIN_BIT
                | LightMen.BRICK_BIT;


        fdef.shape = shape;
        itemBody.createFixture(fdef).setUserData(this);
    }

    @Override
    public void use(Player player) {
        lightmixer.starLight();
        try {
            light.remove(true);
            redlight.remove(true);
            stateTime = 0;
        }catch(NullPointerException ex) {
            ex.printStackTrace();
        }
        destroy();
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        stateTime += deltaTime;
        setPosition(itemBody.getPosition().x - getWidth()/ 2,itemBody.getPosition().y -getHeight()/ 2);
        velocity.y = itemBody.getLinearVelocity().y;
        itemBody.setLinearVelocity(velocity);
        setRegion((TextureRegion) starAnimation.getKeyFrame(stateTime, true));

    }

}
