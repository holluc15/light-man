package com.light.men.Sprites.Blocks;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;
import com.light.men.Constants.GameConstants;
import com.light.men.LightMen;
import com.light.men.Screens.PlayScreen;
import com.light.men.Sprites.Player;

/**
 * Created by Admin on 10.07.2017.
 */

public class JumpBlock extends InteractiveTileObject {

    private box2dLight.PointLight greenLight;
    private box2dLight.PointLight limeLight;
    private Rectangle rect;
    public JumpBlock(PlayScreen screen, MapObject object) {
        super(screen, object);
        rect = ((RectangleMapObject) object).getRectangle();

        body.setUserData("jumpblock");
        setCategoryFilter(LightMen.INTERACTIVE_BIT);

        greenLight = new box2dLight.PointLight(screen.getRayHandler(), GameConstants.rayCount, Color.GREEN,0.325f,(bounds.getX()+bounds.getWidth()/2)/ LightMen.PPM,bounds.getY()/LightMen.PPM);
        limeLight = new box2dLight.PointLight(screen.getRayHandler(),GameConstants.rayCount, Color.LIME,0.157f,(bounds.getX()+bounds.getWidth()/2)/ LightMen.PPM,(bounds.getY()+bounds.getHeight()/2)/LightMen.PPM);
    }



    @Override
    public void onHeadHit(Player player) {
        //do Nothing
    }


    @Override
    public void dispose() {
        greenLight.dispose();
        limeLight.dispose();
    }

    @Override
    public void update() {



    }



}
