package com.light.men.Sprites.MapObjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.light.men.Constants.GameConstants;
import com.light.men.LightMen;
import com.light.men.Screens.PlayScreen;

/**
 * Created by Admin on 20.08.2017.
 */

public class PushBlock extends MapObjects {

    private box2dLight.PointLight grayLight;

    public PushBlock(PlayScreen screen, float x, float y) {
        super(screen, x, y);

        setBounds(getX(),getY(),16 / LightMen.PPM,16 / LightMen.PPM);



        setRegion(new TextureRegion(screen.getAtlas().findRegion("pushblock"), 0, 0, 16, 16));
        velocity.set(0,0);

        grayLight = new box2dLight.PointLight(screen.getRayHandler(), GameConstants.rayCount, Color.GRAY,0.475f,0,0);
        grayLight.setXray(true);
        grayLight.attachToBody(objectBody);
    }

    @Override
    protected void defineObject() {

        BodyDef bdef = new BodyDef();
        bdef.position.set(getX(),getY());
        bdef.type = BodyDef.BodyType.DynamicBody;
        objectBody = world.createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(8/ LightMen.PPM);
        fdef.filter.categoryBits = LightMen.GROUND_BIT;
        fdef.filter.maskBits = LightMen.GROUND_BIT
                | LightMen.ENEMY_BIT
                | LightMen.PLAYER_BIT;

        fdef.shape = shape;
        objectBody.createFixture(fdef).setUserData(this);

    }

    @Override
    public void update(float deltaTime) {





        setPosition(objectBody.getPosition().x - getWidth() / 2, objectBody.getPosition().y - getHeight() / 2);
    }

}
