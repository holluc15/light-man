package com.light.men.Input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.light.men.Constants.GameConstants;
import com.light.men.LightMen;

/**
 * Created by Admin on 21.06.2017.
 */

public class Controller {
    private final Viewport viewport;
    private final Stage stage;
    private boolean upPressed;
    private boolean leftPressed;
    private boolean rightPressed;
    private boolean pressable = true;

    private final SpriteDrawable button1;
    private final SpriteDrawable button1C;
    private final SpriteDrawable button3;
    private final SpriteDrawable button3C;
    private final SpriteDrawable button4;
    private final SpriteDrawable button4C;

    public Controller(Viewport viewport,Stage stage) {

        this.viewport = viewport;
        this.stage = stage;
        Gdx.input.setInputProcessor(this.stage);

        Table tableLeft = new Table();
        Table tableRight = new Table();
        Table tableStrikeRight = new Table();
        tableLeft.setFillParent(true);
        tableRight.setFillParent(true);
        tableRight.right().bottom();
        tableLeft.left().bottom();
        tableStrikeRight.setFillParent(true);
        tableStrikeRight.right().bottom();

        button1 = new SpriteDrawable(new Sprite(new Texture("buttons/controller/jumpButton.png")));
        button1C = new SpriteDrawable(new Sprite(new Texture("buttons/controller/jumpButtonC.png")));
        button3 = new SpriteDrawable(new Sprite(new Texture("buttons/controller/runRightButton.png")));
        button3C = new SpriteDrawable(new Sprite(new Texture("buttons/controller/runRightButtonC.png")));
        button4 = new SpriteDrawable(new Sprite(new Texture("buttons/controller/runLeftButton.png")));
        button4C = new SpriteDrawable(new Sprite(new Texture("buttons/controller/runLeftButtonC.png")));

        final Image upImg = new Image(button1);
        float buttonSize = 40f;
        upImg.setSize(buttonSize, buttonSize);
        upImg.addListener(new InputListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if(pressable)
                upPressed = true;

                upImg.setDrawable(button1C);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                upPressed = false;
                upImg.setDrawable(button1);
            }
        });


        final Image rightImg = new Image(button3);
        rightImg.setSize(buttonSize, buttonSize);
        rightImg.addListener(new InputListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                rightPressed = true;
                rightImg.setDrawable(button3C);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                rightPressed = false;
                rightImg.setDrawable(button3);
            }
        });

        final Image leftImg = new Image(button4);
        leftImg.setSize(buttonSize, buttonSize);
        leftImg.addListener(new InputListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                leftPressed = true;
                leftImg.setDrawable(button4C);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                leftPressed = false;
                leftImg.setDrawable(button4);

            }
        });

        tableRight.add();
        tableRight.pad(0,0,15,50);
        tableRight.add(upImg).size(upImg.getWidth(),upImg.getHeight());
        tableRight.pad(0,0,15,50);
        tableLeft.pad(0,15,25,0);
        tableLeft.add();
        tableLeft.add(leftImg).size(leftImg.getWidth(),leftImg.getHeight());
        tableLeft.add().width(25);
        tableLeft.add(rightImg).size(rightImg.getWidth(),rightImg.getHeight());
        tableLeft.row().padBottom(5);
        tableLeft.add();
        stage.addActor(tableLeft);
        stage.addActor(tableStrikeRight);
        stage.addActor(tableRight);

    }

    public void draw() {
        stage.draw();
    }

    public boolean isUpPressed() {
        return upPressed;
    }

    public boolean isLeftPressed() {
        return leftPressed;
    }

    public boolean isRightPressed() {
        return rightPressed;
    }

    public void setUpNotPressable() {
        pressable = false;
    }


    public void resize(int width,int height) {
        viewport.update(width,height);
    }
}
