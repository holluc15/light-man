package com.light.men.Sprites.Blocks;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.math.Rectangle;
import com.light.men.Constants.GameConstants;
import com.light.men.LightMen;
import com.light.men.Screens.PlayScreen;
import com.light.men.Sprites.Player;

/**
 * Created by Lukas on 09.07.2017.
 */

public class Lantern extends InteractiveTileObject {


    private box2dLight.PointLight goldLight;
    private box2dLight.PointLight otherLight;

    public Lantern(PlayScreen screen, MapObject object) {
        super(screen, object);
        fixture.setUserData(this);
        setCategoryFilter(LightMen.INVISIBLE_BIT);

        goldLight = new box2dLight.PointLight(screen.getRayHandler(), GameConstants.rayCount, Color.GOLD, 0.725f, (bounds.getX() + bounds.getWidth() / 2) / LightMen.PPM, (bounds.getY() + bounds.getHeight() / 2) / LightMen.PPM);
        goldLight.setXray(true);
        otherLight = new box2dLight.PointLight(screen.getRayHandler(), GameConstants.rayCount, new Color(Color.rgb888(255, 145, 137)), 0.2f, (bounds.getX() + bounds.getWidth() / 2) / LightMen.PPM, (bounds.getY() + bounds.getHeight() / 2) / LightMen.PPM);
        otherLight.setXray(true);

    }

    @Override
    public void onHeadHit(Player player) {
        //do nothing
    }


    @Override
    public void dispose() {
        goldLight.dispose();
        otherLight.dispose();

    }

    @Override
    public void update() {

    }

}
