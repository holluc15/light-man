package com.light.men.Sprites.Enemies;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;
import com.light.men.Constants.GameConstants;
import com.light.men.LightMen;
import com.light.men.Screens.PlayScreen;
import com.light.men.Sprites.Player;

/**
 * Created by Admin on 27.07.2017.
 */

public class TankBullet extends Enemy{


    private float stateTime;
    private Animation bulletAnimation;
    private boolean setToDestroy;
    public  boolean  destroyed;
    private Array<TextureRegion> frames;
    float angle;
    private TextureRegion region;
    private String direction;
    private float correctionPixel = 4;

    private box2dLight.ConeLight redlight;
    private Body bulletBody;

    public TankBullet(PlayScreen screen, float x, float y,String direction) {
        super(screen, x, y);
        this.direction = direction;
        frames = new Array<TextureRegion>();
        for(int i = 0; i < 3; i++) {
            frames.add(new TextureRegion(screen.getAtlas().findRegion("tankbullet"), 16*i,0,16,16));
        }

        bulletAnimation = new Animation(0.2f, frames);

        stateTime = 0;
        setBounds(getX(),getY(),16 / LightMen.PPM,16 / LightMen.PPM);
        setToDestroy = false;
        destroyed = false;
        angle = 0;
        region = null;
        if(direction.equals("left")) {
            redlight = new box2dLight.ConeLight(screen.getRayHandler(),GameConstants.rayCount,Color.RED,0.5f,bulletBody.getPosition().x, bulletBody.getPosition().y + 4 / LightMen.PPM, 0, 10);
        }else{
            redlight = new box2dLight.ConeLight(screen.getRayHandler(),GameConstants.rayCount,Color.RED,0.5f,bulletBody.getPosition().x, bulletBody.getPosition().y + 4 / LightMen.PPM, 180, 10);
        }



    }

    @Override
    protected void defineEnemy() {

        BodyDef bdef = new BodyDef();
        bdef.position.set(getX(),getY());
        bdef.type = BodyDef.BodyType.KinematicBody;
        bulletBody = world.createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        PolygonShape shape = new PolygonShape();
        fdef.filter.categoryBits = LightMen.ENEMY_BIT;
        fdef.filter.maskBits = LightMen.GROUND_BIT
                | LightMen.PLAYER_BIT;

        Vector2[] vertice = new Vector2[4];
        vertice[0] = new Vector2(0,8).scl(1/LightMen.PPM);
        vertice[1] = new Vector2(8,0).scl(1/LightMen.PPM);
        vertice[2] = new Vector2(0,0).scl(1/LightMen.PPM);
        vertice[3] = new Vector2(8,8).scl(1/LightMen.PPM);
        shape.set(vertice);
        fdef.shape = shape;
        bulletBody.createFixture(fdef).setUserData(this);
        bulletBody.setBullet(true);


    }

    @Override
    public void update(float deltaTime) {

        stateTime += deltaTime;
        if(setToDestroy && !destroyed) {
           remove();
        }else if(!destroyed) {
            testOutOfSight();
            if(direction.equals("left")) {
                bulletBody.setLinearVelocity(new Vector2(-1.25f,0));
                region = (TextureRegion) bulletAnimation.getKeyFrame(stateTime, true);
                setPosition((bulletBody.getPosition().x - getWidth() / 2)-(correctionPixel-6)/LightMen.PPM, (bulletBody.getPosition().y - getHeight() / 2)+correctionPixel/LightMen.PPM);
                redlight.setPosition(bulletBody.getPosition().x - (correctionPixel*4)/LightMen.PPM,bulletBody.getPosition().y+(correctionPixel*2-correctionPixel)/LightMen.PPM);
            }else if(direction.equals("right")){
                bulletBody.setLinearVelocity(new Vector2(1.25f,0));
                region = (TextureRegion) bulletAnimation.getKeyFrame(stateTime, true);
                setPosition((bulletBody.getPosition().x - getWidth() / 2)+(correctionPixel+2)/LightMen.PPM, (bulletBody.getPosition().y - getHeight() / 2)+correctionPixel/LightMen.PPM);
                redlight.setPosition(bulletBody.getPosition().x + (correctionPixel*4+correctionPixel)/LightMen.PPM,bulletBody.getPosition().y+(correctionPixel*2-correctionPixel)/LightMen.PPM);
            }else{
                System.out.println("WRONG DIRECTION!");
            }

            if(direction.equals("right") && !region.isFlipX()) {
                region.flip(true,false);
            }else if(direction.equals("left") && region.isFlipX()) {
                region.flip(true, false);
            }

            setRegion(region);

        }

    }

    public void remove() {
        world.destroyBody(enemybody);
        destroyed = true;
        stateTime = 0;
        redlight.remove();
        dispose();
    }

    @Override
    public void hitOnHead(Player player) {
        //Do nothing
    }

    @Override
    public void hitByEnemy(Enemy enemy) {
        //do nothing
    }

    public void draw(Batch batch) {
        if(!destroyed ||stateTime < 1) {
            if(region != null) {
                super.draw(batch);
            }

        }
    }


    public void testOutOfSight() {
        if(!screen.getCamera().frustum.pointInFrustum(new Vector3(bulletBody.getPosition().x, bulletBody.getPosition().y, 0)) && bulletBody.getPosition().y < 0) {
            setToDestroy = true;
        }
    }

    public void dispose() {
        redlight.dispose();
    }


}
