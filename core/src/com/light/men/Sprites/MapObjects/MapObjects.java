package com.light.men.Sprites.MapObjects;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.light.men.Screens.PlayScreen;


public abstract class MapObjects extends Sprite {

    final World world;
    final PlayScreen screen;
    public Body objectBody;
    Vector2 velocity;
    protected TiledMap map;

    public MapObjects(PlayScreen screen, float x, float y) {
        this.world = screen.getWorld();
        this.screen = screen;
        this.map = screen.getMap();
        setPosition(x, y);
        defineObject();
        velocity = new Vector2(-1, -2);
        if (objectBody != null) {
            objectBody.setActive(false);
        }


    }

    protected abstract void defineObject();

    public abstract void update(float deltaTime);

    public void reverseVelocity(boolean x, boolean y) {
        if (x) {
            velocity.x = -velocity.x;
        }
        if (y) {
            velocity.y = -velocity.y;
        }
    }
}