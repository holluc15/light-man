package com.light.men.Tools;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.light.men.Constants.GameConstants;
import com.light.men.Items.UseableItems.Emerald;
import com.light.men.Items.ItemDef;
import com.light.men.LightMen;
import com.light.men.Screens.PlayScreen;
import com.light.men.Sprites.Blocks.Brick;
import com.light.men.Sprites.Blocks.Coin;
import com.light.men.Sprites.Blocks.InteractiveTileObject;
import com.light.men.Sprites.MapObjects.MapObjects;
import com.light.men.Sprites.MapObjects.Platform;
import com.light.men.Sprites.Blocks.Spikes;
import com.light.men.Sprites.Enemies.Enemy;
import com.light.men.Sprites.Enemies.Fire;
import com.light.men.Sprites.Enemies.Guard;
import com.light.men.Sprites.Blocks.JumpBlock;
import com.light.men.Sprites.Blocks.Lantern;
import com.light.men.Sprites.Enemies.LaserTank;
import com.light.men.Sprites.MapObjects.PushBlock;
import com.light.men.Sprites.Enemies.UFO;

import java.util.LinkedList;


/**
 * Created by Admin on 21.06.2017.
 */

public class B2WorldCreator {

    private Array<Enemy> enemies;
    private Array<InteractiveTileObject> blocks;
    private Array<MapObjects>mapobjects;



    public B2WorldCreator(PlayScreen screen) {
        World world = screen.getWorld();
        TiledMap map = screen.getMap();

        //Create variables
        BodyDef bdef = new BodyDef();
        PolygonShape shape = new PolygonShape();
        FixtureDef fdef = new FixtureDef();
        Body body;

        blocks = new Array<InteractiveTileObject>();
        enemies = new Array<Enemy>();
        mapobjects = new Array<MapObjects>();

        //Layers
        //0 -> BackgroundColor Layer
        //1 -> Graphic Layer
        //2 -> ObjectGraphic Layer
        //3 -> Ground
        //4 -> Blocks
        //5 -> Enemies
        //6 -> Items


        //create Ground
        for (MapObject object : map.getLayers().get(3).getObjects()) { // Bei get() -> auf die Layers schauen es startet bei 0
            if (object instanceof RectangleMapObject) {
                Rectangle rect = ((RectangleMapObject) object).getRectangle();

                bdef.type = BodyDef.BodyType.StaticBody;
                bdef.position.set((rect.getX() + rect.getWidth() / 2) / LightMen.PPM, (rect.getY() + rect.getHeight() / 2) / LightMen.PPM);

                body = world.createBody(bdef);
                shape.setAsBox(rect.getWidth() / 2 / LightMen.PPM, rect.getHeight() / 2 / LightMen.PPM);
                fdef.shape = shape;
                body.createFixture(fdef);
            } else if (object instanceof PolygonMapObject) {

                Polygon poly = ((PolygonMapObject) object).getPolygon();
                bdef.type = BodyDef.BodyType.StaticBody;
                bdef.position.set(poly.getX() / LightMen.PPM, poly.getY() / LightMen.PPM);

                body = world.createBody(bdef);

                float[] vertices = new float[6];
                float[] array = poly.getVertices();
                for (int i = 0; i < poly.getVertices().length; i++) {
                    vertices[i] = array[i] / LightMen.PPM;
                }
                shape.set(vertices);
                fdef.shape = shape;
                body.createFixture(fdef);

            }

        }


        //create Blocks
        for (MapObject object : map.getLayers().get(4).getObjects().getByType(RectangleMapObject.class)) {
            MapProperties properties = object.getProperties();


            if (properties.containsKey("brick"))
                new Brick(screen, object);
            if (properties.containsKey("coin"))
                new Coin(screen, object);
            if (properties.containsKey("lantern"))
                new Lantern(screen, object);
            if (properties.containsKey("jumpblock"))
                blocks.add(new JumpBlock(screen, object));
            if (properties.containsKey("spike"))
                new Spikes(screen, object);
        }

        //Create Enemies
        if (GameConstants.enemiesActive) {
            for (MapObject object : map.getLayers().get(5).getObjects().getByType(RectangleMapObject.class)) {
                Rectangle rect = ((RectangleMapObject) object).getRectangle();
                MapProperties properties = object.getProperties();

                if (properties.containsKey("guard"))
                    enemies.add(new Guard(screen, rect.getX() / LightMen.PPM, rect.getY() / LightMen.PPM));
                if (properties.containsKey("ufo"))
                    enemies.add(new UFO(screen, rect.getX() / LightMen.PPM, rect.getY() / LightMen.PPM));
                if (properties.containsKey("tank"))
                    enemies.add(new LaserTank(screen, rect.getX() / LightMen.PPM, rect.getY() / LightMen.PPM));
                if (properties.containsKey("fire"))
                    enemies.add(new Fire(screen, rect.getX() / LightMen.PPM, rect.getY() / LightMen.PPM));
            }
        }


        //Create Items
        for (MapObject object : map.getLayers().get(6).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            MapProperties properties = object.getProperties();

            if (properties.containsKey("emerald"))
                screen.spawnItem(new ItemDef(new Vector2(rect.getX() / LightMen.PPM, rect.getY() / LightMen.PPM), Emerald.class));

        }

        for(MapObject object : map.getLayers().get(7).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            MapProperties properties = object.getProperties();

            if (properties.containsKey("platform")) {
                if (properties.containsKey("1")) {
                    mapobjects.add(new Platform(screen, rect.getX() / LightMen.PPM, rect.getY() / LightMen.PPM,1));
                }else{
                    mapobjects.add(new Platform(screen, rect.getX() / LightMen.PPM, rect.getY() / LightMen.PPM,2));
                }
            }
            if (properties.containsKey("pushblock"))
                mapobjects.add(new PushBlock(screen,rect.getX() / LightMen.PPM, rect.getY() / LightMen.PPM));


        }


    }



    public Array<Enemy> getEnemies(){
        return enemies;
    }

    public Array<MapObjects> getMapobjects() {
        return mapobjects;
    }

    public Array<InteractiveTileObject> getBlocks() {
        return blocks;
    }


    public void dispose() {

        for (InteractiveTileObject object: blocks) {
            object.dispose();
        }
       blocks.clear();

    }

}
