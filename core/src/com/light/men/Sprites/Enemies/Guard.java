package com.light.men.Sprites.Enemies;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;
import com.light.men.Constants.GameConstants;
import com.light.men.LightMen;
import com.light.men.Screens.PlayScreen;
import com.light.men.Sprites.Player;


/**
 * Created by Admin on 22.06.2017.
 */


public class Guard extends Enemy {

    private float stateTime;
    private final Animation walkAnimation;
    private boolean setToDestroy;
    public  boolean  destroyed;
    float angle;
    private box2dLight.PointLight redlight;
    private box2dLight.PointLight orangelight;


    public Guard(PlayScreen screen, float x, float y) {
        super(screen, x, y);
        Array<TextureRegion> frames = new Array<TextureRegion>();
        frames.add(new TextureRegion(screen.getAtlas().findRegion("guard1"), 0,0,16,16));
        frames.add(new TextureRegion(screen.getAtlas().findRegion("guard2"), 0,0,16,16));
        walkAnimation = new Animation(0.4f, frames);

        stateTime = 0;
        setBounds(getX(),getY(),16 / LightMen.PPM,16 / LightMen.PPM);
        setToDestroy = false;
        destroyed = false;
        angle = 0;


        redlight = new box2dLight.PointLight(screen.getRayHandler(), GameConstants.rayCount,Color.RED,1.5f,0,0);
        redlight.setIgnoreAttachedBody(true);
        redlight.setXray(true);
        redlight.attachToBody(enemybody);
        orangelight = new box2dLight.PointLight(screen.getRayHandler(),GameConstants.rayCount/2,Color.ORANGE,0.7f,0,0);
        orangelight.setIgnoreAttachedBody(true);
        orangelight.setXray(true);
        orangelight.attachToBody(enemybody);






    }

    public void update(float deltaTime) {

        stateTime += deltaTime;
        if(setToDestroy && !destroyed) {
            redlight.remove();
            orangelight.remove();
            world.destroyBody(enemybody);
            destroyed = true;
            setRegion(new TextureRegion(screen.getAtlas().findRegion("guard3"),0,0,16,16));

            stateTime = 0;
        }else if(!destroyed) {
            enemybody.setLinearVelocity(velocity);
            setPosition(enemybody.getPosition().x - getWidth() / 2, enemybody.getPosition().y - getHeight() / 2);
            setRegion((TextureRegion) walkAnimation.getKeyFrame(stateTime, true));
        }

    }

    @Override
    protected void defineEnemy() {
        BodyDef bdef = new BodyDef();
        bdef.position.set(getX(),getY());
        bdef.type = BodyDef.BodyType.DynamicBody;
        enemybody = world.createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(6/ LightMen.PPM);
        fdef.filter.categoryBits = LightMen.ENEMY_BIT;
        fdef.filter.maskBits = LightMen.GROUND_BIT
                | LightMen.COIN_BIT
                | LightMen.BRICK_BIT
                | LightMen.ENEMY_BIT
                | LightMen.PLAYER_BIT;

        fdef.shape = shape;
        enemybody.createFixture(fdef).setUserData(this);

        //create the Head here
        PolygonShape head = new PolygonShape();
        Vector2[] vertice = new Vector2[4];
        vertice[0] = new Vector2(-5,8).scl(1/LightMen.PPM);
        vertice[1] = new Vector2(5,8).scl(1/LightMen.PPM);
        vertice[2] = new Vector2(-3,3).scl(1/LightMen.PPM);
        vertice[3] = new Vector2(3,3).scl(1/LightMen.PPM);
        head.set(vertice);

        fdef.shape = head;
        fdef.restitution = 0.5f;
        fdef.filter.categoryBits = LightMen.ENEMY_HEAD_BIT;

        enemybody.createFixture(fdef).setUserData(this);
    }

    public void draw(Batch batch) {
        if(!destroyed ||stateTime < 1) {
            super.draw(batch);
        }
    }


    @Override
    public void hitOnHead(Player player) {
        setToDestroy = true;
        //Enemy Death Sound here
    }

    @Override
    public void hitByEnemy(Enemy enemy) {
            //Enemy wird übergeben weil wir später abfragen müssen welcher gegner es war
            reverseVelocity(true, false);
    }




}
