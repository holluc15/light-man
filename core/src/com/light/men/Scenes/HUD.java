package com.light.men.Scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.light.men.LightMen;
import com.light.men.Screens.PlayScreen;


/**
 * Created by Admin on 19.06.2017.
 */

public class HUD implements Disposable{
    public static Stage stage;

    public static Integer worldTimer;
    private float timeCount;

    private final Label countdownLabel;
    private  static Label  fpsLabel;
    private final BitmapFont font;



    public HUD(SpriteBatch sb) {
        worldTimer = 300;
        timeCount = 0;
        Viewport viewport = new FitViewport(LightMen.V_WIDTH, LightMen.V_HEIGHT, new OrthographicCamera());
        stage = new Stage(viewport,sb);


        font = new BitmapFont();
        Table table = new Table();
        table.top(); // allign in the top of our stage
        table.setFillParent(true); // resize the table to our screen size

        countdownLabel = new Label(String.format("%03d",worldTimer),new Label.LabelStyle(font, Color.WHITE));
        fpsLabel = new Label(String.format("%03d",Gdx.graphics.getFramesPerSecond()),new Label.LabelStyle(font, Color.WHITE));
        Label timeLabel = new Label("TIME", new Label.LabelStyle(font, Color.WHITE));
        Label levelLabel = new Label("1-1", new Label.LabelStyle(font, Color.WHITE));
        Label gameLabel = new Label("FPS", new Label.LabelStyle(font, Color.WHITE));
        Label worldLabel = new Label("WORLD", new Label.LabelStyle(font, Color.WHITE));

        table.add(gameLabel).expandX().padTop(10);
        table.add(worldLabel).expandX().padTop(10);
        table.add(timeLabel).expandX().padTop(10);
        table.row();
        table.add(fpsLabel).expandX();
        table.add(levelLabel).expandX();
        table.add(countdownLabel).expandX();

        stage.addActor(table);

    }

    @Override
    public void dispose() {
        stage.dispose();
        font.dispose();
    }

    public void update(float deltaTime) {
        timeCount += deltaTime;
        if(timeCount >= 1) {
            worldTimer--;
            countdownLabel.setText(String.format("%03d",worldTimer));
            timeCount = 0;
        }
    }

    public int getFPS() {
        return Integer.parseInt(fpsLabel.getText()+"");
    }

    public static void updateFPS() {
        fpsLabel.setText(String.format("%03d", Gdx.graphics.getFramesPerSecond()));
    }
}

