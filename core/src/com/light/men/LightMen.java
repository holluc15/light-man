package com.light.men;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.light.men.Screens.LevelSelectionScreen;
import com.light.men.Screens.PlayScreen;
import com.light.men.Screens.StartScreen;

public class LightMen extends Game {

	public SpriteBatch batch;
	public static final int V_WIDTH = 400;
	public static final int V_HEIGHT = 208;
	public static final float PPM = 100;

	public static final short GROUND_BIT = 1;
	public static final short PLAYER_BIT = 2;
	public static final short BRICK_BIT = 4;
	public static final short COIN_BIT = 8;
	public static final short DESTROYED_BIT = 16;
	public static final short PLATFORM_BIT = 32;
	public static final short ENEMY_BIT = 64;
	public static final short ENEMY_HEAD_BIT = 128;
	public static final short ITEM_BIT = 256;
	public static final short PLAYER_HEAD_BIT = 512;
	public static final short FREE_BIT = 1024;
	public static final short INVISIBLE_BIT = 2048;
	public static final short INTERACTIVE_BIT = 4096;

	public static AssetManager manager;


	@Override
	public void create () {
		batch = new SpriteBatch();

		manager = new AssetManager();
		manager.load("audio/music/main_theme.mp3",Music.class);
		manager.load("audio/sounds/coin.wav",Sound.class);
		manager.load("audio/sounds/bump.wav",Sound.class);
		manager.load("audio/sounds/breakblock.wav",Sound.class);
		manager.load("audio/sounds/jump.wav",Sound.class);
		manager.load("audio/sounds/buttonNot.wav",Sound.class);

		manager.finishLoading();

		Music music = manager.get("audio/music/main_theme.mp3", Music.class);
		music.setLooping(true);
		music.play();

		//setScreen(new StartScreen(this));
		//setScreen(new PlayScreen(this,"testmap.tmx"));
		setScreen(new LevelSelectionScreen(this));

	}

	@Override
	public void dispose() {
		super.dispose();
		manager.dispose();
		batch.dispose();
	}




}
