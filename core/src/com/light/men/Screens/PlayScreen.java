package com.light.men.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.light.men.Constants.GameConstants;
import com.light.men.Input.Controller;
import com.light.men.Items.UseableItems.Emerald;
import com.light.men.Items.Item;
import com.light.men.Items.ItemDef;
import com.light.men.Items.UseableItems.Star;
import com.light.men.LightMen;
import com.light.men.Scenes.HUD;
import com.light.men.Sprites.Blocks.InteractiveTileObject;
import com.light.men.Sprites.Enemies.Enemy;
import com.light.men.Sprites.MapObjects.MapObjects;
import com.light.men.Sprites.Player;
import com.light.men.Tools.B2WorldCreator;
import com.light.men.Tools.LightMixer;
import com.light.men.Tools.WorldContactListener;
import java.util.concurrent.LinkedBlockingQueue;

import box2dLight.RayHandler;

public class PlayScreen implements Screen  {

    private final OrthographicCamera camera;
    private final Viewport gamePort;
    private HUD hud;
    //Box2D variables
    private TiledMap map;
    private OrthogonalTiledMapRenderer renderer;

    //Box2D variables
    private World world;
    private B2WorldCreator creator;

    //Init Sprites
    private final TextureAtlas atlas;
    private Player player;

    private final Box2DDebugRenderer b2dr;

    private RayHandler rayHandler;


    private Array<Item> items;
    private LinkedBlockingQueue<ItemDef> itemsToSpawn;


    private LightMen game;
    private String levelname;

    private LightMixer lightmixer;

    private float lightCount;
    private box2dLight.PointLight playerLight;
    private boolean deadJump;
    private Controller controller;



    public PlayScreen(LightMen game,String levelName) {


        atlas = new TextureAtlas("Sprite.pack");
        System.out.println("PlayScreen");

        this.game = game;
        this.levelname = levelName;

        camera = new OrthographicCamera();
        gamePort = new StretchViewport(LightMen.V_WIDTH / LightMen.PPM,LightMen.V_HEIGHT / LightMen.PPM,camera);


        hud = new HUD(game.batch);

        b2dr = new Box2DDebugRenderer();
        controller = new Controller(gamePort,HUD.stage);

        items = new Array<Item>();
        itemsToSpawn = new LinkedBlockingQueue<ItemDef>();

        TmxMapLoader maploader = new TmxMapLoader();
        map = maploader.load("levels/"+levelName);
        renderer = new OrthogonalTiledMapRenderer(map, 1 / LightMen.PPM);
        camera.position.set(gamePort.getWorldWidth()/2,gamePort.getWorldHeight()/2,0);

        world = new World(new Vector2(0, -10), true); // 1 Parameter Gravity

        rayHandler = new RayHandler(world);

        
        creator = new B2WorldCreator(this);


        player = new Player(this);
        playerLight = new box2dLight.PointLight(rayHandler,GameConstants.rayCount, Color.PURPLE,0.7f,0,0);
        playerLight.attachToBody(player.b2body);
        playerLight.setIgnoreAttachedBody(true);
        if(GameConstants.playerShadow)
            playerLight.setXray(true);
        else
            playerLight.setXray(false);



        lightmixer = new LightMixer(this);
        deadJump = false;


        world.setContactListener(new WorldContactListener());
    }

    public void spawnItem(ItemDef itemDef) {
        itemsToSpawn.add(itemDef);
    }

    private void handleSpawningItems() {
        if(!itemsToSpawn.isEmpty()) {
            ItemDef itemDef = itemsToSpawn.poll();
            if(itemDef.type == Star.class) {
                items.add(new Star(this,itemDef.position.x,itemDef.position.y));
            }else if(itemDef.type == Emerald.class){
                items.add(new Emerald(this,itemDef.position.x,itemDef.position.y));
            }
        }
    }

    public Player getPlayer() {
        return player;
    }

    public TextureAtlas getAtlas() {
        return atlas;
    }

    public TiledMap getMap() {
        return map;
    }

    public World getWorld() {
        return world;
    }

    public OrthographicCamera getCamera() {
        return camera;
    }

    public RayHandler getRayHandler() {
        return rayHandler;
    }

    public Controller getController() {
        return controller;
    }


    private void handleInput() {

        if(player.currentState != Player.State.DEAD) {

            if(controller.isRightPressed() && player.b2body.getLinearVelocity().x <= 2)
                player.b2body.applyLinearImpulse(new Vector2(0.1f,0),player.b2body.getWorldCenter(),true);
            if(controller.isLeftPressed() && player.b2body.getLinearVelocity().x >= -2)
                player.b2body.applyLinearImpulse(new Vector2(-0.1f,0),player.b2body.getWorldCenter(),true);
            if(controller.isUpPressed())
                player.jump();
        }


    }


    private void update(float deltaTime) {
        //check if there is a input from the player

            handleInput();
            handleSpawningItems();

            world.step(1 / 60f, 6, 2);

            player.update(deltaTime);




        for(Enemy enemy : creator.getEnemies()) {
            enemy.update(deltaTime);
            if(enemy.getX() < player.getX() + 224 / LightMen.PPM) { //224
                enemy.enemybody.setActive(true);
            }
        }

        for(MapObjects object : creator.getMapobjects()) {
            object.update(deltaTime);
            if(object.getX() < player.getX() + 224 / LightMen.PPM) {
                object.objectBody.setActive(true);
            }
        }

        if(GameConstants.playerStarLight) {
            lightCount += deltaTime;
            lightmixer.animStarLight(lightCount);
            if(lightCount >= 10) {
                GameConstants.playerStarLight = false;
                playerLight.remove();
                playerLight = lightmixer.setPlayerLightNormal();
                playerLight.setIgnoreAttachedBody(true);
                playerLight.attachToBody(player.b2body);
                lightCount = 0;
            }
        }


            for (Item item : items) {
                item.update(deltaTime);
            }

            for(InteractiveTileObject object: creator.getBlocks()) {
                object.update();
            }

            player.testOutOfView();
            HUD.updateFPS();
            if(hud.getFPS() < 30) {
              System.gc();
            }

            hud.update(deltaTime);

        if(player.currentState != Player.State.DEAD) {
            camera.position.x = player.b2body.getPosition().x;
        }

            camera.update();
            renderer.setView(camera);

        }

    @Override
    public void render(float deltaTime) {

            update(deltaTime);

            Gdx.gl.glClearColor(0, 0, 0, 0);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

            renderer.render();


            if(GameConstants.renderLines) {
                b2dr.render(world, camera.combined);
            }

            game.batch.setProjectionMatrix(camera.combined);

        if(GameConstants.lights) {
            rayHandler.setCombinedMatrix(camera);
            rayHandler.updateAndRender();
        }
            game.batch.begin();

            player.draw(game.batch);



        if(GameConstants.enemiesActive) {
            for (Enemy enemy : creator.getEnemies()) {
                enemy.draw(game.batch);
            }
        }

        for (MapObjects object : creator.getMapobjects()) {
            object.draw(game.batch);
        }

            for (Item item : items) {
                item.draw(game.batch);
            }


            game.batch.end();

            game.batch.setProjectionMatrix(HUD.stage.getCamera().combined);
            HUD.stage.draw();
            controller.draw();

        if(gameOver()) {
            game.setScreen(new GameOverScreen(game,levelname));
            dispose();
        }

        if(GameConstants.renderOnlyLines) {
            b2dr.render(world, camera.combined);
        }

        if(!GameConstants.isPlaying) {
            game.setScreen(new LevelSelectionScreen(game));
            dispose();
        }

    }

    public boolean gameOver() {
        if(player.currentState == Player.State.DEAD && player.getStateTimer() > 1f) {
            return true;
        }else if(player.currentState == Player.State.DEAD && player.getStateTimer() < 1f){
            if(!deadJump) {
                player.b2body.applyLinearImpulse(new Vector2(0, 2f), player.b2body.getWorldCenter(), true);
                deadJump = true;
                //Play Death Sound
            }
        }
        return false;
    }


    @Override
    public void resize(int width, int height) {
        gamePort.update(width,height);
        controller.resize(width,height);
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        creator.dispose();
        map.dispose();
        renderer.dispose();
        world.dispose();
        b2dr.dispose();
        hud.dispose();
        lightmixer.dispose();

    }

    @Override
    public void show() {

    }



}