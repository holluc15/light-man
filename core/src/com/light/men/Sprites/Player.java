package com.light.men.Sprites;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.light.men.Constants.GameConstants;
import com.light.men.Input.Controller;
import com.light.men.LightMen;
import com.light.men.Scenes.HUD;
import com.light.men.Screens.PlayScreen;


public class Player extends Sprite{

    private final World world;
    public Body b2body;
    private final TextureRegion playerStand;
    private final TextureRegion playerDead;

    public enum State {
        FALLING,JUMPING,STANDING,RUNNING,DEAD
    }

    public State currentState;
    public State previousState;
    private final Animation playerRun;
    private final Animation playerJump;

    private boolean runningRight;
    private float stateTimer;
    private final OrthographicCamera camera;
    private Controller controller;

    public Player(PlayScreen screen) {

        super(screen.getAtlas().findRegion("player"));
        this.world = screen.getWorld();
        currentState = State.STANDING;
        previousState = State.STANDING;
        stateTimer = 0;
        runningRight = true;
        camera = screen.getCamera();
        controller = screen.getController();


        Array<TextureRegion> frames = new Array<TextureRegion>();

        for(int i = 1; i < 4; i++) {
            frames.add(new TextureRegion(screen.getAtlas().findRegion("player"),i*16,0,16,16));
        }
        playerRun = new Animation(0.1f,frames);
        frames.clear();

        //add Jump Animation
        frames.add(new TextureRegion(screen.getAtlas().findRegion("player"),64,0,16,16));

        playerJump = new Animation(0.1f,frames);


        definePlayer();
        playerStand = new TextureRegion(screen.getAtlas().findRegion("player"),0,0,16,16); // x start y start size size
        playerDead = new TextureRegion(screen.getAtlas().findRegion("player"), 80, 0, 16, 16);
        setBounds(20,20,16 / LightMen.PPM,16 / LightMen.PPM);
        setRegion(playerStand);



    }

    private void definePlayer() {
        BodyDef bdef = new BodyDef();
        bdef.position.set(200/ LightMen.PPM,32/ LightMen.PPM);
        bdef.type = BodyDef.BodyType.DynamicBody;
        b2body = world.createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(6/ LightMen.PPM);

        fdef.filter.categoryBits = LightMen.PLAYER_BIT;
        fdef.filter.maskBits = LightMen.GROUND_BIT
                | LightMen.COIN_BIT
                | LightMen.BRICK_BIT
                | LightMen.ENEMY_BIT
                | LightMen.ENEMY_HEAD_BIT
                | LightMen.ITEM_BIT
                | LightMen.INTERACTIVE_BIT
                | LightMen.PLATFORM_BIT;


        fdef.shape = shape;
        b2body.createFixture(fdef);

        EdgeShape head = new EdgeShape();
        head.set(new Vector2(-2/ LightMen.PPM, 6 / LightMen.PPM),new Vector2(2/ LightMen.PPM, 6 / LightMen.PPM));
        fdef.filter.categoryBits = LightMen.PLAYER_HEAD_BIT;
        fdef.shape = head;
        fdef.isSensor = true;
        b2body.createFixture(fdef);
        b2body.setUserData("player");


    }

    public void update(float deltaTime) {
        setPosition(b2body.getPosition().x -getWidth()/2,b2body.getPosition().y - getHeight()/2);
        setRegion(getFrame(deltaTime));

    }



    private TextureRegion getFrame(float deltaTime) {

        currentState = getState();

        TextureRegion region = null;


            switch(currentState) {
                case DEAD:
                    region = playerDead;
                    break;
                case JUMPING:
                    region = (TextureRegion) playerJump.getKeyFrame(stateTimer); break;
                case RUNNING:
                    region = (TextureRegion) playerRun.getKeyFrame(stateTimer,true); break;
                case FALLING:
                case STANDING:
                    region = playerStand; break;

            }


        if((controller.isLeftPressed()|| !runningRight) && !region.isFlipX()) { // b2body.getLinearVelocity().x < 0
            region.flip(true,false); // Dreht die Texture
            runningRight = false;

        }else if((controller.isRightPressed() || runningRight) && region.isFlipX()) {
            region.flip(true,false);
            runningRight = true;
        }

        stateTimer = currentState == previousState ? stateTimer + deltaTime: 0;

        previousState = currentState;

        return region;
    }


    private State getState() {

        if(GameConstants.playerIsDead) {
            return State.DEAD;
        }else if(b2body.getLinearVelocity().y > 0 || (b2body.getLinearVelocity().y < 0 && previousState == State.JUMPING)) {
            return State.JUMPING;
        }
        else if(b2body.getLinearVelocity().y < 0) {
            return State.JUMPING;
        }else if(controller.isLeftPressed() || controller.isRightPressed()) {
            return State.RUNNING;
        }else{
            return State.STANDING;
        }
    }

    public float getStateTimer() {
        return stateTimer;
    }

    public void testOutOfView() {

        if(!camera.frustum.pointInFrustum(new Vector3(b2body.getPosition().x, b2body.getPosition().y, 0)) && b2body.getPosition().y < 0) {

            if(HUD.worldTimer != 300) {
                GameConstants.playerIsDead = true;
            }

        }
    }


    public void jump() {
        System.out.println(currentState);
        System.out.println(b2body.getLinearVelocity());
        if (currentState != State.JUMPING && !GameConstants.playerJumpsHigher) {
            b2body.applyLinearImpulse(new Vector2(0, 4f), b2body.getWorldCenter(), true);
            LightMen.manager.get("audio/sounds/jump.wav",Sound.class).play();
        }
        if(currentState != State.JUMPING && GameConstants.playerJumpsHigher) {
            b2body.applyLinearImpulse(new Vector2(0, 5.25f), b2body.getWorldCenter(), true);
            LightMen.manager.get("audio/sounds/jump.wav",Sound.class).play();
        }
        currentState = State.JUMPING;


    }










}
