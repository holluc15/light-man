package com.light.men.Sprites.Enemies;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;
import com.light.men.Constants.GameConstants;
import com.light.men.LightMen;
import com.light.men.Screens.PlayScreen;
import com.light.men.Sprites.Player;

/**
 * Created by Admin on 27.07.2017.
 */

public class UFO extends Enemy{

    private float stateTime;
    private float deathState;
    private Animation flyAnimation;
    private Animation deathAnimation;
    private boolean setToDestroy;
    public  boolean  destroyed;
    private Array<TextureRegion> frames;
    private box2dLight.ConeLight blueLight;
    private box2dLight.ConeLight orangeLight;


    public UFO(PlayScreen screen, float x, float y) {
        super(screen, x, y);

        frames = new Array<TextureRegion>();
        for(int i = 0; i < 4; i++) {
            frames.add(new TextureRegion(screen.getAtlas().findRegion("ufo"), 16*i,0,16,16));
        }
        flyAnimation = new Animation(0.4f, frames);
        frames.clear();
        for(int i = 4; i < 10; i++) {
            frames.add(new TextureRegion(screen.getAtlas().findRegion("ufo"), 16*i,0,16,16));
        }
        deathAnimation = new Animation(0.1f, frames);

        blueLight = new box2dLight.ConeLight(screen.getRayHandler(), GameConstants.rayCount, Color.BLUE, 0.75f, enemybody.getPosition().x, enemybody.getPosition().y + 4 / LightMen.PPM, -90, 36);
        orangeLight = new box2dLight.ConeLight(screen.getRayHandler(), GameConstants.rayCount, Color.GREEN, 0.75f, enemybody.getPosition().x, enemybody.getPosition().y + 4 / LightMen.PPM, -90, 36);

        stateTime = 0;
        deathState = 0;
        setBounds(getX(),getY(),16 / LightMen.PPM,16 / LightMen.PPM);
        setToDestroy = false;
        destroyed = false;
        velocity = new Vector2(-0.5f,0);


    }

    @Override
    protected void defineEnemy() {

        BodyDef bdef = new BodyDef();
        bdef.position.set(getX(),getY());
        bdef.type = BodyDef.BodyType.DynamicBody;
        enemybody = world.createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(1/ LightMen.PPM);
        fdef.filter.categoryBits = LightMen.ENEMY_BIT;
        fdef.filter.maskBits = LightMen.GROUND_BIT
                | LightMen.COIN_BIT
                | LightMen.BRICK_BIT
                | LightMen.ENEMY_BIT
                | LightMen.PLAYER_BIT
                | LightMen.INVISIBLE_BIT;

        fdef.shape = shape;
        enemybody.createFixture(fdef).setUserData(this);

        //create the Laser here
        PolygonShape laser = new PolygonShape();
        Vector2[] vertice = new Vector2[3];
        vertice[0] = new Vector2(0,0).scl(1/LightMen.PPM);
        vertice[1] = new Vector2(32f,-48).scl(1/LightMen.PPM);
        vertice[2] = new Vector2(-32f,-48).scl(1/LightMen.PPM);
        laser.set(vertice);

        fdef.shape = laser;
        fdef.restitution = 0.5f;
        fdef.filter.categoryBits = LightMen.ENEMY_BIT;

        enemybody.createFixture(fdef).setUserData(this);

        //create the Head here
        CircleShape head = new CircleShape();
        head.setRadius(7/LightMen.PPM);

        fdef.shape = head;
        fdef.restitution = 0.5f;
        fdef.filter.categoryBits = LightMen.ENEMY_HEAD_BIT;

        enemybody.createFixture(fdef).setUserData(this);

    }

    @Override
    public void update(float deltaTime) {
        stateTime += deltaTime;
        if(setToDestroy && !destroyed) {
            world.destroyBody(enemybody);
            blueLight.remove();
            orangeLight.remove();
            destroyed = true;
            stateTime = 0;
            deathState = 0;
        }else if(!destroyed) {


            enemybody.setLinearVelocity(velocity);
            blueLight.setPosition(enemybody.getPosition().x,enemybody.getPosition().y);
            orangeLight.setPosition(enemybody.getPosition().x,enemybody.getPosition().y);

            setPosition(enemybody.getPosition().x - getWidth() / 2, enemybody.getPosition().y - getHeight() / 2);
            setRegion((TextureRegion) flyAnimation.getKeyFrame(stateTime, true));

        }
        if(destroyed) {
            deathState += deltaTime;
            enemybody.setLinearVelocity(new Vector2(0,0));
            setRegion((TextureRegion) deathAnimation.getKeyFrame(deathState, true));
            setPosition(enemybody.getPosition().x - getWidth() / 2, enemybody.getPosition().y - getHeight() / 2);

        }

    }

    @Override
    public void hitOnHead(Player player) {
       setToDestroy = true;

    }

    @Override
    public void hitByEnemy(Enemy enemy) {
        reverseVelocity(true,false);
    }

    public void draw(Batch batch) {
        if(!destroyed || stateTime < 1) {
            super.draw(batch);
        }
    }
}
