package com.light.men.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.light.men.Constants.GameConstants;
import com.light.men.LightMen;

import box2dLight.PointLight;
import box2dLight.RayHandler;

/**
 * Created by Admin on 31.07.2017.
 */

public class StartScreen extends Sprite implements Screen {

    private Viewport viewport;
    private Stage stage;
    private float buttonSizeX,buttonSizeY;

    private Label textLabel;
    private float stateTime;
    private final SpriteDrawable textImg;
    Image text;
    private Game game;


    public StartScreen(Game game) {

        System.out.println("START");
        this.game = game;

        viewport = new StretchViewport(LightMen.V_WIDTH, LightMen.V_HEIGHT, new OrthographicCamera());
        stage = new Stage(viewport, ((LightMen)game).batch);
        Gdx.input.setInputProcessor(stage);


        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("ui/fonts/guiFont.TTF"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 32;
        parameter.characters = FreeTypeFontGenerator.DEFAULT_CHARS;
        parameter.borderWidth = 3;
        parameter.incremental = true;
        parameter.shadowOffsetX = 2;
        BitmapFont bitmapFont = generator.generateFont(parameter);
        generator.dispose();
        Label.LabelStyle font = new Label.LabelStyle(bitmapFont,Color.WHITE);


        textLabel = new Label("LightMen",font);
        textLabel.setSize(GameConstants.col_width,GameConstants.row_height);
        textImg = new SpriteDrawable(new Sprite(new Texture("buttons/menuButtons/buttonText.png")));
        createGUI();
        stateTime = 0;



    }

    public void createGUI() {


        text = new Image(textImg);
        text.setSize(buttonSizeX,buttonSizeY);

        Table background = new Table();
        background.setFillParent(true);
        background.setBackground(new SpriteDrawable(new Sprite(new Texture("ui/startBackground.png"))));

        Table UI = new Table();
        UI.setSize(GameConstants.col_width, GameConstants.row_height);
        UI.add(textLabel).pad(-175, textLabel.getWidth() / 2 + 250, 0, textLabel.getWidth() / 2);
        UI.row();
        UI.add(text).pad(-50, text.getWidth() / 2 + 250, 0, text.getWidth() / 2);

        stage.addActor(background);
        stage.addActor(UI);


    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        stateTime += delta;
        if(Gdx.input.justTouched()) {
            GameConstants.playerIsDead = false;
            GameConstants.playerStarLight = false;
            game.getScreen().dispose();
            game.setScreen(new LevelSelectionScreen((LightMen)game));
            dispose();
        }
        if((int)stateTime % 2 == 0 ) {
            text.setVisible(false);
        }else{
            text.setVisible(true);
        }
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
