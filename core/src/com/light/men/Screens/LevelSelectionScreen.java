package com.light.men.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.light.men.Constants.GameConstants;
import com.light.men.LightMen;

public class LevelSelectionScreen implements Screen {
    private Viewport viewport;
    private Stage stage;
    private float buttonSizeX,buttonSizeY;

    private final SpriteDrawable leftButton,rightButton,backButton,okButton;
    private final SpriteDrawable leftButtonC,rightButtonC,backButtonC,okButtonC;
    private Image levelImg;

    private Game game;
    private String level;
    private int levelCount;
    private int levelAnz = 3;

    public LevelSelectionScreen(Game game){
        this.game = game;
        viewport = new StretchViewport(LightMen.V_WIDTH, LightMen.V_HEIGHT, new OrthographicCamera());
        stage = new Stage(viewport, ((LightMen) game).batch);
        Gdx.input.setInputProcessor(stage);

        leftButton = new SpriteDrawable(new Sprite(new Texture("buttons/menuButtons/leftButton.png")));
        rightButton = new SpriteDrawable(new Sprite(new Texture("buttons/menuButtons/rightButton.png")));
        backButton = new SpriteDrawable(new Sprite(new Texture("buttons/menuButtons/backButton.png")));
        okButton = new SpriteDrawable(new Sprite(new Texture("buttons/menuButtons/okButton.png")));
        leftButtonC = new SpriteDrawable(new Sprite(new Texture("buttons/menuButtons/leftButtonC.png")));
        rightButtonC = new SpriteDrawable(new Sprite(new Texture("buttons/menuButtons/rightButtonC.png")));
        backButtonC = new SpriteDrawable(new Sprite(new Texture("buttons/menuButtons/backButtonC.png")));
        okButtonC = new SpriteDrawable(new Sprite(new Texture("buttons/menuButtons/okButtonC.png")));
        levelImg = new Image(new SpriteDrawable(new Sprite(new Texture("ui/levels/level1.png"))));
        levelImg.setSize(buttonSizeX,buttonSizeY);
        createGUI();
        levelCount = 1;
        level = getLevel(levelCount);


    }

    public void createGUI() {


        final Image buttonL = new Image(leftButton);
        buttonL.setSize(buttonSizeX, buttonSizeY);
        buttonL.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                buttonL.setDrawable(leftButtonC);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

                buttonL.setDrawable(leftButton);
                if(levelCount > 0)
                    levelCount--;
                update();
            }
        });

        final Image buttonR = new Image(rightButton);
        buttonR.setSize(buttonSizeX, buttonSizeY);
        buttonR.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {


                buttonR.setDrawable(rightButtonC);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

                buttonR.setDrawable(rightButton);
                if(levelCount < levelAnz)
                    levelCount++;
                update();
            }
        });

        final Image backButtonImg = new Image(backButton);
        backButtonImg.setSize(buttonSizeX, buttonSizeY);
        backButtonImg.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {


                backButtonImg.setDrawable(backButtonC);

                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

                backButtonImg.setDrawable(backButton);
                game.setScreen(new StartScreen((LightMen)game));

            }
        });

        final Image okButtonImg = new Image(okButton);
        okButtonImg.setSize(buttonSizeX, buttonSizeY);
        okButtonImg.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {

                okButtonImg.setDrawable(okButtonC);

                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

                okButtonImg.setDrawable(okButton);
                GameConstants.isPlaying = true;
                game.setScreen(new PlayScreen((LightMen)game,getLevel(levelCount)));

            }
        });

        Table background = new Table();
        background.setFillParent(true);
        background.setBackground(new SpriteDrawable(new Sprite(new Texture("ui/background.png"))));
        stage.addActor(background);

        Table back = new Table();
        back.bottom().left();
        back.setFillParent(true);
        back.add(backButtonImg).center().padTop(10);
        stage.addActor(back);

        Table ok = new Table();
        ok.bottom().right();
        ok.setFillParent(true);
        ok.add(okButtonImg).center().padTop(10);
        stage.addActor(ok);

        Table level = new Table();
        level.center();
        level.setFillParent(true);
        level.add(levelImg).center().padTop(10);
        stage.addActor(level);

        Table right = new Table();
        right.center().right();
        right.setFillParent(true);
        right.add(buttonR).center().padTop(10);
        stage.addActor(right);


        Table left = new Table();
        left.center().left();
        left.setFillParent(true);
        left.add(buttonL).center().padTop(10);
        stage.addActor(left);

    }

    public String getLevel(int stage) {
        switch (stage) {
            case 0: return "testmap.tmx";
            case 1: return "level1.tmx";
            case 2: return "level2.tmx";
            case 3: return "level3.tmx";
        }
        return "testmap.tmx";

    }

    public Image getLevelImg() {
        return new Image(new SpriteDrawable(new Sprite(new Texture("ui/levels/level"+levelCount+".png"))));
    }

    @Override
    public void show() {

    }


    public void update() {
        levelImg = getLevelImg();
       createGUI();
    }

    @Override
    public void render(float delta) {


        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();

    }
}
