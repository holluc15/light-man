package com.light.men.Items;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.light.men.LightMen;
import com.light.men.Screens.PlayScreen;
import com.light.men.Sprites.Player;

/**
 * Created by Admin on 22.06.2017.
 */

public abstract class Item extends Sprite {
    public final PlayScreen screen;
    public final World world;
    public boolean toDestroy;
    public Vector2 velocity;
    public boolean destroyed;
    public Body itemBody;

    public Item(PlayScreen screen, float x, float y) {
        this.screen = screen;
        this.world = screen.getWorld();
        toDestroy = false;
        destroyed = false;

        setPosition(x,y);
        setBounds(getX(),getY(),16 / LightMen.PPM,16 / LightMen.PPM);
        defineItem();

    }

    protected abstract void defineItem();
    public abstract void use(Player player);

    public void stopVelocity() {
        velocity.x = 0;
        velocity.y = 0;
        velocity.setZero();
    }

    public void update(float deltaTime) {
        if(toDestroy && !destroyed) {
            world.destroyBody(itemBody);
            destroyed = true;
        }
    }

    public void draw(Batch batch) {
        if(!destroyed) {
            super.draw(batch);
        }
    }

    public void destroy() {
        toDestroy = true;
    }

    public void reverseVelocity(boolean x,boolean y) {
        if(x) {
            velocity.x = -velocity.x;
        }
        if(y) {
            velocity.y = -velocity.y;
        }
    }
}
