package com.light.men.Tools;


import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.light.men.Constants.GameConstants;
import com.light.men.Items.Item;
import com.light.men.Sprites.Enemies.Enemy;
import com.light.men.Sprites.Blocks.InteractiveTileObject;
import com.light.men.Sprites.MapObjects.Platform;
import com.light.men.Sprites.Player;

import static com.light.men.LightMen.*;


/**
 * Created by Admin on 22.06.2017.
 */

public class WorldContactListener implements ContactListener{

    @Override
    public void beginContact(Contact contact) {


        Fixture fixA = contact.getFixtureA();
        Fixture fixB = contact.getFixtureB();

        if(fixA.getFilterData().categoryBits == INTERACTIVE_BIT || fixB.getFilterData().categoryBits == INTERACTIVE_BIT) {
            String userDataA = fixA.getBody().getUserData().toString();
            String userDataB = fixB.getBody().getUserData().toString();

            if(userDataA.equals("jumpblock") && userDataB.equals("player")) {
                GameConstants.playerJumpsHigher = true;
            }
            if(userDataB.equals("jumpblock") && userDataA.equals("player")) {
                GameConstants.playerJumpsHigher = true;
            }
        }


        int cDef = fixA.getFilterData().categoryBits | fixB.getFilterData().categoryBits;

        switch(cDef) {

            case PLAYER_HEAD_BIT | BRICK_BIT:
            case PLAYER_HEAD_BIT | COIN_BIT:
                if (fixA.getFilterData().categoryBits == PLAYER_HEAD_BIT)
                    ((InteractiveTileObject) fixB.getUserData()).onHeadHit((Player) fixA.getUserData());
                else
                    ((InteractiveTileObject) fixA.getUserData()).onHeadHit((Player) fixB.getUserData());
                break;

            case ENEMY_HEAD_BIT | PLAYER_BIT:
                if (fixA.getFilterData().categoryBits == ENEMY_HEAD_BIT)
                    ((Enemy) fixA.getUserData()).hitOnHead((Player) fixB.getUserData());
                else
                    ((Enemy) fixB.getUserData()).hitOnHead((Player) fixA.getUserData());
                break;


            case ENEMY_BIT | COIN_BIT:
            case ENEMY_BIT | BRICK_BIT:
            case ENEMY_BIT | GROUND_BIT:
            case ENEMY_BIT | INVISIBLE_BIT:
                if (fixA.getFilterData().categoryBits == ENEMY_BIT)
                    ((Enemy) fixA.getUserData()).reverseVelocity(true, false);
                else
                    ((Enemy) fixB.getUserData()).reverseVelocity(true, false);
                break;

            case PLAYER_BIT | ENEMY_BIT:
                GameConstants.playerIsDead = true;
                break;

            case ENEMY_BIT | ENEMY_BIT:
                ((Enemy) fixA.getUserData()).hitByEnemy((Enemy) fixB.getUserData());
                ((Enemy) fixB.getUserData()).hitByEnemy((Enemy) fixA.getUserData());
                break;

            case ITEM_BIT | GROUND_BIT:
                if (fixA.getFilterData().categoryBits == ITEM_BIT)
                    ((Item) fixA.getUserData()).stopVelocity();
                else
                    ((Item) fixB.getUserData()).stopVelocity();
                break;
            case ITEM_BIT | PLAYER_BIT:
                if (fixA.getFilterData().categoryBits == ITEM_BIT)
                    ((Item) fixA.getUserData()).use((Player) fixB.getUserData());
                else
                    ((Item) fixB.getUserData()).use((Player) fixA.getUserData());
                break;

            case PLAYER_BIT | PLATFORM_BIT:
                if (fixA.getFilterData().categoryBits == PLATFORM_BIT) {
                    ((Platform) fixA.getUserData()).start();
                }else{
                ((Platform) fixB.getUserData()).start();
                }
                break;
        }
    }

    @Override
    public void endContact(Contact contact) {


        Fixture fixA = contact.getFixtureA();
        Fixture fixB = contact.getFixtureB();

        if(fixA.getFilterData().categoryBits == INTERACTIVE_BIT || fixB.getFilterData().categoryBits == INTERACTIVE_BIT) {
            String userDataA = fixA.getBody().getUserData().toString();
            String userDataB = fixB.getBody().getUserData().toString();

            if(userDataA.equals("jumpblock") && userDataB.equals("player")) {
                GameConstants.playerJumpsHigher = false;
            }
            if(userDataB.equals("jumpblock") && userDataA.equals("player")) {
                GameConstants.playerJumpsHigher = false;
            }
        }

        int cDef = fixA.getFilterData().categoryBits | fixB.getFilterData().categoryBits;

        switch(cDef) {
            case PLAYER_BIT | PLATFORM_BIT:
                if(fixA.getFilterData().categoryBits == PLATFORM_BIT && ((Platform) fixA.getUserData()).getType() == 1)
                        ((Platform) fixA.getUserData()).stop();
                else if(fixB.getFilterData().categoryBits == PLATFORM_BIT && ((Platform) fixB.getUserData()).getType() == 1)
                    ((Platform) fixB.getUserData()).stop();
                break;

        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
