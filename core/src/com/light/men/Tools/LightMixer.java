package com.light.men.Tools;

import com.badlogic.gdx.graphics.Color;
import com.light.men.Constants.GameConstants;
import com.light.men.Screens.PlayScreen;
import com.light.men.Sprites.Player;

import box2dLight.RayHandler;

/**
 * Created by Lukas on 07.07.2017.
 */

public class LightMixer {

    private box2dLight.PointLight yellowLight;
    private box2dLight.PointLight redLight;
    private Player player;
    private RayHandler rayHandler;


    public LightMixer(PlayScreen screen) {

        rayHandler = screen.getRayHandler();
        player = screen.getPlayer();
        yellowLight = new box2dLight.PointLight(rayHandler,GameConstants.rayCount, Color.YELLOW,0.45f,0,0);
        redLight = new box2dLight.PointLight(rayHandler,GameConstants.rayCount, Color.RED,0.525f,0,0);
        yellowLight.setActive(false);
        redLight.setActive(false);

    }

    public void starLight() {

        yellowLight.setActive(true);
        yellowLight.setIgnoreAttachedBody(true);

        redLight.setActive(true);
        redLight.setIgnoreAttachedBody(true);

        GameConstants.playerStarLight = true;


    }

    public void animStarLight(float counter) {

        if((int)counter % 2 == 0) {
            yellowLight.setActive(true);
            yellowLight.setDistance(2f);
            yellowLight.setIgnoreAttachedBody(true);
            yellowLight.attachToBody(player.b2body);
            redLight.setDistance(2f);


        }else{
            redLight.setActive(true);
            yellowLight.setDistance(0.45f);
            yellowLight.setIgnoreAttachedBody(true);
            yellowLight.attachToBody(player.b2body);
            redLight.setDistance(0.525f);
            redLight.setIgnoreAttachedBody(true);
            redLight.attachToBody(player.b2body);
        }

    }


    public box2dLight.PointLight setPlayerLightNormal() {
        GameConstants.playerStarLight = false;
        redLight.setActive(false);
        yellowLight.setActive(false);


        return new box2dLight.PointLight(rayHandler, GameConstants.rayCount, Color.PURPLE, 0.7f, 0, 0);
    }



    public void dispose() {
        if(redLight != null && yellowLight != null) {
            redLight.dispose();
            yellowLight.dispose();
        }

    }




}
