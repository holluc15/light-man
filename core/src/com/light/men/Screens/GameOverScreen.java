package com.light.men.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.light.men.Constants.GameConstants;
import com.light.men.LightMen;


public class GameOverScreen implements Screen {
    private Viewport viewport;
    private Stage stage;
    private float buttonSizeX,buttonSizeY;

    private Label textLabel;
    private final SpriteDrawable buttonEnd,buttonEndC,buttonRestartC,buttonRestart,buttonMenuC,buttonMenu;

    private Game game;
    private String level;

    public GameOverScreen(Game game,String level){
        this.game = game;
        this.level = level;
        viewport = new StretchViewport(LightMen.V_WIDTH, LightMen.V_HEIGHT, new OrthographicCamera());
        stage = new Stage(viewport, ((LightMen) game).batch);

        Label.LabelStyle font = new Label.LabelStyle(new BitmapFont(), Color.WHITE);

        textLabel = new Label("You DIED!",font);
        textLabel.setSize(GameConstants.col_width,GameConstants.row_height);
        buttonEnd = new SpriteDrawable(new Sprite(new Texture("buttons/menuButtons/exitButton.png")));
        buttonEndC = new SpriteDrawable(new Sprite(new Texture("buttons/menuButtons/exitButtonC.png")));
        buttonRestart = new SpriteDrawable(new Sprite(new Texture("buttons/menuButtons/restartButton.png")));
        buttonRestartC = new SpriteDrawable(new Sprite(new Texture("buttons/menuButtons/restartButtonC.png")));
        buttonMenu = new SpriteDrawable(new Sprite(new Texture("buttons/menuButtons/menuButton.png")));
        buttonMenuC = new SpriteDrawable(new Sprite(new Texture("buttons/menuButtons/menuButtonC.png")));
        createGUI();


    }

    public void createGUI() {


        final Image endButton = new Image(buttonEndC);
        endButton.setSize(buttonSizeX, buttonSizeY);
        endButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {


                endButton.setDrawable(buttonEnd);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

                endButton.setDrawable(buttonEndC);
                System.exit(0);
            }
        });


        final Image restartButton = new Image(buttonRestartC);
        restartButton.setSize(buttonSizeX, buttonSizeY);
        restartButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {

                restartButton.setDrawable(buttonRestart);

                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

                restartButton.setDrawable(buttonRestartC);
                GameConstants.playerIsDead = false;
                GameConstants.playerStarLight = false;
                game.setScreen(new PlayScreen((LightMen) game,level));
                dispose();
            }
        });

        final Image menuButton = new Image(buttonMenuC);
        menuButton.setSize(buttonSizeX, buttonSizeY);
        menuButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {

                menuButton.setDrawable(buttonMenu);

                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

                menuButton.setDrawable(buttonMenuC);

            }
        });

        Table background = new Table();
        background.setFillParent(true);
        background.setBackground(new SpriteDrawable(new Sprite(new Texture("ui/deathBackground.png"))));

        Table UI = new Table();
        UI.setSize(GameConstants.col_width, GameConstants.row_height);
        UI.add(textLabel).pad(-75, textLabel.getWidth() / 2 + 250, 0, textLabel.getWidth() / 2);
        UI.row();
        UI.add(restartButton).pad(0, restartButton.getWidth() / 2 + 250, 0, restartButton.getWidth() / 2);

        stage.addActor(background);
        stage.addActor(UI);


    }

    @Override
    public void show() {

    }



    @Override
    public void render(float delta) {
        if(Gdx.input.justTouched()) {
            GameConstants.playerIsDead = false;
            GameConstants.playerStarLight = false;
            game.getScreen().dispose();
            game.setScreen(new PlayScreen((LightMen) game,level));
            dispose();
        }
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        System.gc();
    }
}
