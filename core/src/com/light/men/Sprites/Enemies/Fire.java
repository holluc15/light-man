package com.light.men.Sprites.Enemies;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;
import com.light.men.Constants.GameConstants;
import com.light.men.LightMen;
import com.light.men.Screens.PlayScreen;
import com.light.men.Sprites.Player;

/**
 * Created by Admin on 28.07.2017.
 */

public class Fire extends Enemy{

    private float stateTime;
    private Animation fireAnimation;
    private boolean setToDestroy;
    public  boolean  destroyed;
    private Array<TextureRegion> frames;
    private box2dLight.PointLight light;
    private float timer;

    public Fire(PlayScreen screen, float x, float y) {
        super(screen, x, y);
        frames = new Array<TextureRegion>();
        for (int i = 0; i < 3; i++) {
            frames.add(new TextureRegion(screen.getAtlas().findRegion("fire"), 16 * i, 0, 16, 16));
        }
        fireAnimation = new Animation(0.1f, frames);

        stateTime = 0;
        setBounds(getX(), getY(), 16 / LightMen.PPM, 16 / LightMen.PPM);
        setToDestroy = false;
        destroyed = false;

        light = new box2dLight.PointLight(screen.getRayHandler(), GameConstants.rayCount, Color.RED, 0.3f, 0, 0);
        light.setIgnoreAttachedBody(true);
        light.attachToBody(enemybody);
        light.setXray(true);
        light.setSoft(true);
        timer = 0;


    }


    @Override
    protected void defineEnemy() {
        BodyDef bdef = new BodyDef();
        bdef.position.set(getX() + 8/LightMen.PPM, getY());
        bdef.type = BodyDef.BodyType.StaticBody;
        enemybody = world.createBody(bdef);
        FixtureDef fdef = new FixtureDef();

        //create the Triangle here
        PolygonShape tri = new PolygonShape();
        Vector2[] vertice = new Vector2[3];
        vertice[0] = new Vector2(0, 12f).scl(1 / LightMen.PPM);
        vertice[1] = new Vector2(4f, 0).scl(1 / LightMen.PPM);
        vertice[2] = new Vector2(-4f, 0).scl(1 / LightMen.PPM);
        tri.set(vertice);

        fdef.shape = tri;
        fdef.restitution = 0.5f;
        fdef.filter.categoryBits = LightMen.ENEMY_BIT;

        enemybody.createFixture(fdef).setUserData(this);
    }

    @Override
    public void update(float deltaTime) {
        stateTime += deltaTime;
        timer +=deltaTime;

            setPosition(enemybody.getPosition().x - 8 / LightMen.PPM, enemybody.getPosition().y);
            setRegion((TextureRegion) fireAnimation.getKeyFrame(stateTime, true));
        if(timer <= 0.5) {
            light.setColor(Color.RED);
            light.setDistance(0.3f);
        }else{
            light.setColor(Color.ORANGE);
            light.setDistance(0.4f);
            timer = 0;
        }
    }

    public void draw(Batch batch) {
        if(!destroyed ||stateTime < 1 && enemybody.isActive()) {
                super.draw(batch);
        }
    }

    @Override
    public void hitOnHead(Player player) {
        //Do nothing
    }

    @Override
    public void hitByEnemy(Enemy enemy) {
        reverseVelocity(true,false);
    }

    public void dispose() {
        light.dispose();

    }
}
