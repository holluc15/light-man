package com.light.men.Sprites.Blocks;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.math.Rectangle;
import com.light.men.Constants.GameConstants;
import com.light.men.LightMen;
import com.light.men.Screens.PlayScreen;
import com.light.men.Sprites.Player;

/**
 * Created by Admin on 23.07.2017.
 */

public class Spikes extends InteractiveTileObject{

    private box2dLight.PointLight grayLight;
    private box2dLight.PointLight darkgrayLight;
    public Spikes(PlayScreen screen, MapObject object) {
        super(screen, object);

        fixture.setUserData(this);
        setCategoryFilter(LightMen.ENEMY_BIT);

        grayLight = new box2dLight.PointLight(screen.getRayHandler(), GameConstants.rayCount/2, Color.LIGHT_GRAY,0.175f,(bounds.getX()+bounds.getWidth()/2)/ LightMen.PPM,(bounds.getY()+bounds.getHeight()/2)/LightMen.PPM);
        darkgrayLight = new box2dLight.PointLight(screen.getRayHandler(),GameConstants.rayCount, Color.DARK_GRAY,0.125f,(bounds.getX()+bounds.getWidth()/2)/ LightMen.PPM,(bounds.getY()+bounds.getHeight()/2)/LightMen.PPM);

    }

    @Override
    public void onHeadHit(Player player) {
        //do nothing
    }

    @Override
    public void dispose() {
        grayLight.dispose();
        darkgrayLight.dispose();
    }

    @Override
    public void update() {

    }




}
